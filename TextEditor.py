#!  /usr/bin/python3.4

import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
from tkinter import *

class TextEditor():
    """A simple text editing application"""
    def __init__(self, master):
        """A simple text editing application.
       Constructor.        
        """
        self._master = master
        self._master.title("Text Editor")
        self._master.geometry('800x500+100+150')

       
        self._filename = ''
        self._is_edited = False

        self._text = tk.Text(master, background = 'mint cream')
        self._text.pack(side=tk.TOP, expand=True, fill=tk.BOTH)
        self._text.bind("<Key>", self._set_edited)

        # Create the menu
        menubar = tk.Menu(master, background = 'dark slate gray')
        master.config(menu=menubar)

        filemenu = tk.Menu(menubar, background = 'dark sea green')
        menubar.add_cascade(label="File", menu=filemenu)
        filemenu.add_command(label="New", command=self.new)
        filemenu.add_command(label="Open", command=self.open_file)
        filemenu.add_command(label="Save", command=self.save)
        filemenu.add_command(label="Save As...", command=self.save_as)
        filemenu.add_command(label="Exit", command=self.close)
      
        
        self.myTools = Frame(self._master)
        self.myTools.pack(side = 'top', fill = 'x')
        
        self.newButton = Button(master, text='New',  bg="aquamarine4", command=self.new).pack(side=LEFT)
        self.openButton = Button(master, text='Open',  bg="aquamarine4", command=self.open_file).pack(side=LEFT)
        self.saveButton = Button(master, text='Save', bg="aquamarine4", command=self.save).pack(side=LEFT)
        self.saveasButton = Button(master, text='Save As', bg="aquamarine4"
                                            ,command=self.save_as).pack(side=LEFT)


        helpmenu = tk.Menu(menubar, background = 'dark sea green')
        menubar.add_cascade(label="Help", menu=helpmenu)
        helpmenu.add_command(label="About", command=self.about)

        master.protocol("WM_DELETE_WINDOW", self.close)

    def new(self):
        """Create a new file.               
        """
        if self._can_close():
            self._text.delete(1.0, tk.END)
            self._filename = ''
            self._master.title("Text Editor")
            self._is_edited = False

    def open_file(self):
        """Open a file.        
        texteditor.open_file()        
        """
        if not self._can_close():
            return

        self._filename = filedialog.askopenfilename()
        if self._filename:
            f = open(self._filename, "r")
            text = f.read()
            f.close()

            self._text.delete(1.0, tk.END)
            self._text.insert(tk.END, text)
            self._master.title("Text Editor: {0}".format(self._filename))
            self._is_edited = False

    def save(self):
        """Perform the "Save" functionality.               
        """
        if not self._filename:
            self._filename = filedialog.asksaveasfilename()
        self._perform_save()

    def save_as(self):
        """Perform the "Save As..." functionality.               
        """
        filename = filedialog.asksaveasfilename()
        if filename:
            self._filename = filename
        self._perform_save()

    def close(self):
        """Exit the application.                
        """
        if self._can_close():
            self._master.destroy()

    def about(self):
        """Generate an 'About' dialog.               
        """
        messagebox.showinfo(title="Text Editor", message="A simple text editor")

    def _set_edited(self, event):
        """Record that the text file has been edited.               
        """
        self._is_edited = True

    def _perform_save(self):
        """The functionality behind "Save" and "Save As...".          
        """
        if self._filename:
            self._master.title("Text Editor: {0}".format(self._filename))
            f = open(self._filename, "w")
            text = self._text.get(1.0, tk.END)[:-1]
            f.write(text)
            f.close()
            self._is_edited = False

    def _can_close(self):
        """Check if the file needs to be saved.
        Return True if it is safe to close this file,
        return False if the user wants to continue editing.
        """
        if self._is_edited:
            reply = messagebox.askquestion(type=messagebox.YESNOCANCEL,
                        title="File not saved!",
                        message="Would you like to save this file?")
            if reply == messagebox.YES:
                self.save()
                return True
            elif reply == messagebox.NO:
                return True
            elif reply == messagebox.CANCEL:
                return False
        else:
            return True


root = tk.Tk()
TextEditor(root)
root.mainloop()




